import atheris
import struct
import sys

# import and instrument the library to be fuzzed
with atheris.instrument_imports():
    import biolib as bio

# instrument the relevant function
@atheris.instrument_func
def test_count_kmers(input_bytes):
    fdp = atheris.FuzzedDataProvider(input_bytes)
    k = fdp.ConsumeUInt(1)
    dna = []
    # generate a DNA string from random bytes
    for c in fdp.ConsumeBytes(atheris.ALL_REMAINING):
        dna.append('atcg'[c>>6])
        dna.append('atcg'[(c>>4)&0x3])
        dna.append('atcg'[(c>>2)&0x3])
        dna.append('atcg'[c&0x3])
    bio.count_kmers(''.join(dna), k)

atheris.Setup(sys.argv, test_count_kmers)
atheris.Fuzz()
