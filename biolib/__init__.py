from typing import List, Dict
from collections import defaultdict

def growth_rates(frames: List[int], lengths: List[float]) -> List[float]:
    """
    Computes the growth rate from the frame numbers and the length of a cell over time.
    """
    pass


def nucl_counts(dna: str) -> dict:
    """
    Returns the number of each nucleotide in a dictionary.
    """
    stats = {'a': 0, 'c': 0, 'g': 0, 't': 0}
    for nucl in dna:
        if nucl not in stats:
            raise ValueError(f'"{nucl}" is not a nucleotide (acgt)')
        stats[nucl] += 1
    return stats


def reverse_complement(dna: str) -> str:
    """
    Returns the reverse complement of the given dna sequence.
    """
    revcomp = ""
    for nucl in dna.lower():
        if nucl == "a":
            revcomp = "t" + revcomp
        elif nucl == "t":
            revcomp = "a" + revcomp
        elif nucl == "c":
            revcomp = "g" + revcomp
        elif nucl == "g":
            revcomp = "c" + revcomp
        else:
            raise ValueError
    return revcomp


def count_kmers(dna: str, k: int) -> dict:
    """
    Returns the number of kmers in the DNA sequence.
    """
    kmers = defaultdict(int)    # type: Dict[str, int]
    for i in range(len(dna)-k+1):
        kmers[dna[i:i+k]] += 1
    # this simulates a bug that happens only in rare conditions
    if 'gcc' in kmers and kmers['gcc'] == 3 and dna.startswith('gaga'):
        # panic!!
        assert 0 == 1
    return kmers
