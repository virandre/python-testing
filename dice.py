"""
# A funny use of a fuzzer.

There exist non-transitive sets of dice, i.e. such that when played
against each other, the die A wins most of the time against the die B,
B wins most of the time against C, and C wins most of the time against A.

It is not trivial to construct such a set of dice, but it is easy with
a fuzzer!  Just launch it on a function that crashes if the dice given
as inputs have this property, and it will adapt the input to find the
"bug".

Just launch this file with python to start the fuzzer:

    python3 dice.py

It searches by default for three 4-sided dice with sides that can be
numbered from 0 to 4 included.

When it finds a non-transitive set of dice, it writes the binary input
to a file named `crash-****`.  To see the numbers, launch the program
again with the option `--show` and the name of the crash file:

    python3 dice.py --show crash-*

"""

import atheris
import sys
import numpy as np


@atheris.instrument_func
def check_transitivity(die1: list, die2: list, die3: list):
    if SHOW:
        print(die1, die2, die3)
    die1_beats_die2 = sum(np.sign(b - a) for a in die1 for b in die2)
    die2_beats_die3 = sum(np.sign(b - a) for a in die2 for b in die3)
    die3_beats_die1 = sum(np.sign(b - a) for a in die3 for b in die1)
    # this fails if die1 > die2 > die3 > die1
    assert not (die1_beats_die2 > 0 and die2_beats_die3 > 0 and die3_beats_die1 > 0)
    # this fails if die1 < die2 < die3 < die1
    assert not (die1_beats_die2 < 0 and die2_beats_die3 < 0 and die3_beats_die1 < 0)


@atheris.instrument_func
def fuzz(input_bytes):
    fdp = atheris.FuzzedDataProvider(input_bytes)
    # 4-sided dice with numbers from 0 to 4 included
    die1 = fdp.ConsumeIntListInRange(4, 0, 4)
    die2 = fdp.ConsumeIntListInRange(4, 0, 4)
    die3 = fdp.ConsumeIntListInRange(4, 0, 4)
    check_transitivity(die1, die2, die3)


if "--show" in sys.argv:
    sys.argv.remove("--show")
    SHOW = True
else:
    SHOW = False

atheris.Setup(sys.argv, fuzz)
atheris.Fuzz()
