# python-testing

This is a small repo to showcase the use of several testing frameworks in
Python:

* static typing with `mypy`
* unit testing with `pytest`
* property-based testing with `hypothesis`
* fuzzing with `atheris`
* coverage reports with `coverage`
* linting with `bandit`, `pyflakes` and `pylint`

All these libraries need to be installed, either with `pip` or `conda`
(`atheris` is only installable with `pip` at the moment).

## Static typing

Type annotations are not enforced by Python, but they are exploited by
third-party linters and IDEs.  It is the case of `mypy`, which can check
statically (i.e. without running anything) single files as well as whole
modules.  It can be slow the first time when checking big imported modules.

    mypy mypy.py

## Test coverage

The package `coverage` "counts" checks which lines of the source code have
been run during the execution of a program.  If we run it during the tests,
we know what lines of the source code are untested.  The goal is to reach
100% coverage of the source code by the test suite, even though of course
this does not guarantee the absence of bugs.

    coverage run -m pytest  # Run pytest with coverage analysis
    coverage report -m      # Print the coverage report

## Linting

In case the IDE or text editor doesn't perform linting in real time, `bandit`,
`pyflakes` and `pylint` are three of the most used linters and can report
deviations from good coding practice (along with a certain number of false
positives).

## Unit testing

Tests are in the file `test_basic.py`.  They run each time that `pytest`
is launched on the command line.  They test edge cases or well crafted
input/output pairs covering the scope of behaviors of the functions.

## Property-based testing

Property-based testing is another form of unit testing where the inputs
are generated randomly, according to the types of the arguments.  This
is the opportunity to check for *invariants*: properties that hold
true for every input/output.  Also, if there exists an alternative
implementation of the function being tested, it can be an idea to
compare the two outputs to verify that the two functions are equivalent.

The main property-based testing library in Python is `hypothesis`, and
the tests are located in the file `test_hypothesis.py`.  They are also
run by `pytest`.

## Fuzzing

Unlike property-based testing which tests the function with fully random
inputs, (instrumentation-based) fuzzing engines specifically hunt for
bugs.  They place flags in the code and search for inputs that activate
these flags, aiming to activate all of them (to cover all the range of
behaviors of the code).  In doing so, they can trigger bugs located on
code paths hard to reach and not covered by manual tests.

The kind of bugs uncovered by fuzzing are internal errors of the program
being tested: hard crashes, failed assertions or uncaught exceptions.

Several fuzzing libraries exist, usually aimed at compiled languages
(C or C++).  Google recently published `atheris`, a fuzzing library
for Python, making use of LLVM's `libFuzzer`.  The tests are located
in `fuzzing.py` and are run by explicitely running the file (`python
fuzzing.py`).  This command only stops if and when it finds a bug.

Larger and more complex codebases might have fuzzers running continuously
on dedicated servers, and setup to exploit previously found bugs in order
to find new ones more easily.

