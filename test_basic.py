import biolib as bio

def test_reverse_complement_empty():
    assert bio.reverse_complement('') == ''

def test_reverse_complement_lowercase():
    assert bio.reverse_complement('atcg') == 'cgat'

def test_reverse_complement_uppercase():
    assert bio.reverse_complement('ATCG') == 'cgat'


def test_count_kmers_empty():
    assert bio.count_kmers('', 4) == {}

def test_count_kmers_1():
    assert bio.count_kmers('acgcgt', 1) == {'a': 1, 'c': 2, 'g': 2, 't': 1}

def test_count_kmers_2():
    assert bio.count_kmers('acgcgt', 2) == {'ac': 1, 'cg': 2, 'gc': 1, 'gt': 1}

