import biolib

MIN_PER_FRAME = 5.0

frames = [22, 23, 24, 25]
lengths = [20.0, 20.5, 21.2, 22.6]

# calling the function properly with a list of frame numbers
grs = biolib.growth_rates(frames, lengths)
print('right')

times = [frame * MIN_PER_FRAME for frame in frames]

# calling the function improperly with a list of times
# mypy with realize that a list[float] is passed instead of a list[int]
# and will return an error
grs = biolib.growth_rates(times, lengths)
print('wrong')
