from hypothesis import given, example
import hypothesis.strategies as st
import biolib as bio


@given(st.text(alphabet='atgc'))
def test_nucl_counts_dict(dna):
    stats = bio.nucl_counts(dna)
    assert set(stats.keys()) == set(list('atgc'))

@given(st.text(alphabet='atgc'))
def test_nucl_counts_sum(dna):
    stats = bio.nucl_counts(dna)
    assert len(dna) == sum(stats.values())

@given(st.text(alphabet='atgc'))
def test_nucl_counts_counts(dna):
    stats = bio.nucl_counts(dna)
    for nucl in 'atgc':
        assert stats[nucl] == dna.count(nucl)

@given(st.text())
def test_nucl_counts_alltext(dna):
    try:
        stats = bio.nucl_counts(dna)
    except ValueError:
        pass


@given(st.text(alphabet='atgcATGC'))
def test_reverse_complement_length(dna):
    assert len(bio.reverse_complement(dna)) == len(dna)

@given(st.text(alphabet='atgcATGC'))
def test_reverse_complement_involution(dna):
    assert bio.reverse_complement(bio.reverse_complement(dna)) == dna.lower()


@given(dna=st.text(alphabet='atgc'), k=st.integers(min_value=0))
def test_count_kmers_sum(dna, k):
    kmers = bio.count_kmers(dna, k)
    assert sum(kmers.values()) == max(len(dna) - k + 1, 0)

@given(dna=st.text(alphabet='atgc'), k=st.integers(min_value=0))
def test_count_kmers_length(dna, k):
    kmers = bio.count_kmers(dna, k)
    for kmer in kmers:
        assert len(kmer) == k

